package Services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import Models.Incident;

public class IncidentService {

	//Constructeur permettant d'instancier notre classe
	public IncidentService() {
		
	}
	
	public List<Incident> getAllIncidents() {
		
		//Fonction permettant de renvoyer une liste d'incident � l'aide du JSON retourn� par notre WebService
		List<Incident> incidents = new ArrayList<Incident>();
		try {
			//Url du webSevice
			String dataUrl = "http://vps295091.ovh.net/php/allIncidents.php";
			
			//Cr�ation de l'URL
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            
            //On envoie une requ�te HTTP sur l'url avec la m�thode GET
            
            //Autorisation des donn�es en entr�e et en sortie 
            httpUrlConnection.setRequestMethod("GET");
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            
            //On d�finit un InputStream permettant de lire l'ensemble du contenu de l'URL
            InputStream IN = httpUrlConnection.getInputStream();
            
            //Permet de lire ce contenu
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            
            //Construction d'une chaine de caract�re du contenu
            while((line = bufferedReader.readLine()) != null){
                res += line;
            }
            bufferedReader.close();
            IN.close();
            
            //D�connexion de l'urel
            httpUrlConnection.disconnect();
      
            try {
            	
            	//On r�cup�re le contenu de la chaine construite auparavant que l'on met dans un JSONARRAY
            	JSONArray array =  new JSONArray(res);
            	
            	//On it�re sur ce tableau de contenu JSON
            	for (int i = 0; i < array.length(); i++) {
            		
            		//On r�cup�re chaque objet JSON contenu dans ce tableau
            		JSONObject jsonObject = array.getJSONObject(i);
            		
            		//On r�cup�re chaque �l�ment contenu dans l'objet JSON permettant de caster dans l'objet Incident
            		int id_incident = Integer.parseInt(jsonObject.getString("id_incident"));
            		String name_incident = jsonObject.getString("name_incident");
            		String description_inc = jsonObject.getString("description_inc");
            		String statement_inc = jsonObject.getString("statement_inc");
            		String libelle_pro = jsonObject.getString("libelle_pro");
            		int id_pro = Integer.parseInt(jsonObject.getString("id_pro"));
            		String priority_inc = jsonObject.getString("priority_inc");
            		Float progress_inc = Float.valueOf(jsonObject.getString("progress_inc"));
            		int id_user = Integer.parseInt(jsonObject.getString("id_userAssign"));
            		String prenom_user = jsonObject.getString("prenom_user");
            		String nom_user = jsonObject.getString("nom_user");
            		int id_module = Integer.parseInt(jsonObject.getString("id_module"));
            		String name_module = jsonObject.getString("name_module");
            		
            		Incident bug = new Incident(
            				id_incident, 
            				name_incident, 
            				description_inc, 
            				statement_inc, 
            				priority_inc, 
            				id_user,
            				prenom_user + " " + nom_user, 
            				id_pro, 
            				libelle_pro, 
            				id_module,
            				name_module,
            				progress_inc); 
            		
            		//On ajoute l'incident � la liste que l'on va renvoyer
            		incidents.add(bug);

            	}

            } catch (Exception e){
            	e.printStackTrace();
            }
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return incidents;
		
	}
	
	//Fonction permettant d'ajouter un incident
	public String addIncident (String libelle_inc, String description_inc, String statement_inc, int id_userReport, int id_userAssign, int id_pro, int id_module, String priority_inc, Float progress_inc) {

		try {
			//Url du webService
			String dataUrl = "http://vps295091.ovh.net/php/addIncident.php";
			
			//Connexion � l'url
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setDoOutput(true);
            
            //Permet depasser les donn�es
            OutputStream OS = httpUrlConnection.getOutputStream();
            
            BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            
            //Construction des donn�es que l'on va envoyer au web Service
            String data = URLEncoder.encode("libelle_inc", "UTF-8") + "=" +URLEncoder.encode(libelle_inc, "UTF-8") + "&" +
                    URLEncoder.encode("description_inc", "UTF-8") + "=" +URLEncoder.encode(description_inc, "UTF-8") + "&" +
                    URLEncoder.encode("statement_inc", "UTF-8") + "=" +URLEncoder.encode(statement_inc, "UTF-8") + "&" +
                    URLEncoder.encode("id_userReport", "UTF-8") + "=" +URLEncoder.encode(Integer.toString(id_userReport),"UTF-8") + "&" + 
                    URLEncoder.encode("id_userAssign", "UTF-8") + "=" + URLEncoder.encode(Integer.toString(id_userAssign), "UTF-8") + "&" +
                    URLEncoder.encode("id_pro", "UTF-8") + "=" + URLEncoder.encode(Integer.toString(id_pro), "UTF-8") + "&" +
                    URLEncoder.encode("id_module", "UTF-8") + "=" + URLEncoder.encode(Integer.toString(id_module), "UTF-8") + "&" +
                    URLEncoder.encode("priority_inc", "UTF-8") + "=" + URLEncoder.encode(priority_inc, "UTF-8") + "&" +
                    URLEncoder.encode("progress_inc", "UTF-8") + "=" + URLEncoder.encode(Float.toString(progress_inc), "UTF-8");
            
            bufferwriter.write(data);
            bufferwriter.flush();
            bufferwriter.close();
            OS.close();
            
            //Permet de lire le contenu 
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            
            //Construction du r�sultat
            while((line = bufferedReader.readLine()) != null){
                res += line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();

            return res;
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
            
	
	}

	//Fonction permettant de mettre � jour un incident
	public String updateIncident(int id_inc, String libelle_inc, String description_inc, String statement_inc, int id_userAssign, Float progress_inc) {
		try {
			
			//Ouverture de l'URL pour le webService
			String dataUrl = "http://vps295091.ovh.net/php/updateIncident.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            
            OutputStream OS = httpUrlConnection.getOutputStream();
            
            //On construit les donn�es que l'on envoie, ce sera les $_POST de l'url
            BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = URLEncoder.encode("id_inc", "UTF-8") + "=" + URLEncoder.encode(Integer.toString(id_inc),"UTF-8") + "&" +
                    URLEncoder.encode("libelle_inc", "UTF-8") + "=" + URLEncoder.encode(libelle_inc,"UTF-8") + "&" +
                    URLEncoder.encode("description_inc", "UTF-8") + "=" + URLEncoder.encode(description_inc,"UTF-8") + "&" +
                    URLEncoder.encode("statement_incat", "UTF-8") + "=" + URLEncoder.encode(statement_inc,"UTF-8") + "&" +
                    URLEncoder.encode("id_userAssign", "UTF-8") + "=" + URLEncoder.encode(Integer.toString(id_userAssign),"UTF-8") + "&" +
                    URLEncoder.encode("progress_inc", "UTF-8") + "=" + URLEncoder.encode(Float.toString(progress_inc), "UTF-8");
            
            bufferwriter.write(data);
            bufferwriter.flush();
            bufferwriter.close();
            OS.close();
            
            //R�cup�ration des donn�es pour l'url avec les donn�es pass�es en param�tres
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res += line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();

            return res;
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	//Fonction qui permet de supprimer un incident 
	public String deleteIncident(int id_inc) {
		try {
			
			//On ouvre l'url du webService
			String dataUrl = "http://vps295091.ovh.net/php/DeleteIncident.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            
            //D�finition du type de la requ�te
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            
            OutputStream OS = httpUrlConnection.getOutputStream();
            
            //Construction des donn�es que l'on envoie au webService
            BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = URLEncoder.encode("id_inc", "UTF-8") + "=" + URLEncoder.encode(Integer.toString(id_inc),"UTF-8");
            
            bufferwriter.write(data);
            bufferwriter.flush();
            bufferwriter.close();
            OS.close();
            
            //On r�cup�re le contenu de l'url avec les donn�es que l'on a envoy�es
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            
            //Construction de la chaine de caract�re, r�sultat renvoyer par la requ�te HTTP
            while((line = bufferedReader.readLine()) != null){
                res += line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();

            return res;
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	//Fonction qui permettra de supprimer l'ensemble des incidents pour le module 
	public String deleteIncidentByModule(int id_module) {
		try {
			
			//On ouvre l'url du webService associ� 
			String dataUrl = "http://vps295091.ovh.net/php/deleteAllIncidentsByModule.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            
            //D�finition du type de la requ�te HTTP
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            
            OutputStream OS = httpUrlConnection.getOutputStream();
            
            //On envoie les donn�es au Web Service
            BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = URLEncoder.encode("id_module", "UTF-8") + "=" + URLEncoder.encode(Integer.toString(id_module),"UTF-8");
            
            bufferwriter.write(data);
            bufferwriter.flush();
            bufferwriter.close();
            OS.close();
            
            //R�cup�ration du contenu de la requ�te
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res += line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();

            return res;
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
