package Services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import Models.Team;

public class TeamService {

	public TeamService() {
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) {
		
	}
	
	public List<String> getAllNameTeams(List<Team> teams) {
		List<String> names = new ArrayList<String>();
		
		for (Team team : teams) {
			names.add(team.getName_team());
		}
		return names;
	}
	
	public List<Team> getAllTeams() {
		
		List<Team> teams = new ArrayList<Team>();
		try {
			
			String dataUrl = "http://vps295091.ovh.net/php/allTeams.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("GET");
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res += line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();
            System.out.println(res);
      
            try {
            	JSONArray array =  new JSONArray(res);
            	
            	for (int i = 0; i < array.length(); i++) {
            		
            		JSONObject jsonObject = array.getJSONObject(i);
            		
            		int id_team = Integer.parseInt(jsonObject.getString("id"));
            		String name_team = jsonObject.getString("name");
            		
            		Team team = new Team(id_team, name_team);
            		
            		teams.add(team);
            		
            	}

            } catch (Exception e){
            	e.printStackTrace();
            }
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return teams;
		
	}
	
	public String addTeam (String name_team) {

		try {
			
			String dataUrl = "http://vps295091.ovh.net/php/addTeam.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setDoOutput(true);
            
            OutputStream OS = httpUrlConnection.getOutputStream();
            
            BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = URLEncoder.encode("name_team", "UTF-8") + "=" + URLEncoder.encode(name_team,"UTF-8");
            
            bufferwriter.write(data);
            bufferwriter.flush();
            bufferwriter.close();
            OS.close();
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res+=line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();
            
            System.out.println(res);
            
            return res;
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
	
	public String updateTeam(int id_team, String name_team) {
		try {
			
			String dataUrl = "http://vps295091.ovh.net/php/updateTeam.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            
            OutputStream OS = httpUrlConnection.getOutputStream();
            
            BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = URLEncoder.encode("isAdmin", "UTF-8") + "=" +URLEncoder.encode(String.valueOf(id_team),"UTF-8") + "&" +
                    URLEncoder.encode("description", "UTF-8") + "=" +URLEncoder.encode(name_team,"UTF-8");
            
            bufferwriter.write(data);
            bufferwriter.flush();
            bufferwriter.close();
            OS.close();
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res += line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();
            System.out.println(res);
            return res;
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String deleteTeam(int id_team) {
		try {
			
			String dataUrl = "http://vps295091.ovh.net/php/deleteTeam.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            
            OutputStream OS = httpUrlConnection.getOutputStream();
            
            BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = URLEncoder.encode("id_Team", "UTF-8") + "=" + URLEncoder.encode(Integer.toString(id_team),"UTF-8");
            
            bufferwriter.write(data);
            bufferwriter.flush();
            bufferwriter.close();
            OS.close();
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res+=line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();
            
            System.out.println(res);
            return res;
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
