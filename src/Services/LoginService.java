package Services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONObject;

import sun.net.www.protocol.http.HttpURLConnection;




public class LoginService {

	public LoginService() {
		// TODO Auto-generated constructor stub
	}
	
	//Fonction qui permet de v�rifier que les identifiants pass�s par le client sont bons, v�rifie �galement si le user est admin
	public Boolean launchLoginService(String email, String password, String admin) {
		try {
			
			//On ouvre l'url : webService associ�
			String dataUrl = "http://vps295091.ovh.net/php/loginAdmin.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            
            //On lui passe les donn�es qui seront fournies par l'utilisateur
            OutputStream OS = httpUrlConnection.getOutputStream();
            BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = URLEncoder.encode("mail", "UTF-8") + "=" + URLEncoder.encode(email,"UTF-8") + "&" +
            		URLEncoder.encode("mdp", "UTF-8") + "=" + URLEncoder.encode(password,"UTF-8") + "&" +
            		URLEncoder.encode("admin", "UTF-8") + "=" + URLEncoder.encode(admin,"UTF-8");
            bufferwriter.write(data);
            bufferwriter.flush();
            bufferwriter.close();
            OS.close();
            
            //R�cup�ration du r�sultat de la requ�te HTTP
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res += line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();
            
            //On r�cup�re le r�sultat de l'objet JSON obtenu
            JSONArray array =  new JSONArray(res);

    		JSONObject jsonObject = array.getJSONObject(0);
    		
    		String code = jsonObject.getString("code");
    		
    		return code.equals("0");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
            
	}
}
