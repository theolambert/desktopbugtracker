package Services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import Models.Team;
import Models.User;

public class UserService {

	public UserService() {
		// TODO Auto-generated constructor stub
	}

	public List<User> getAllUsers() {
		
		List<User> users = new ArrayList<User>();
		try {
			
			String dataUrl = "http://vps295091.ovh.net/php/allUsers.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("GET");
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res += line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();
            System.out.println(res);
      
            try {
            	JSONArray array =  new JSONArray(res);
            	
            	for (int i = 0; i < array.length(); i++) {
            		
            		JSONObject jsonObject = array.getJSONObject(i);
            		
            		int id_user = Integer.parseInt(jsonObject.getString("id_user"));
            		String nom_user = jsonObject.getString("nom_user");
            		String prenom_user = jsonObject.getString("prenom_user");
            		String email_user = jsonObject.getString("email_user");
            		String name_team = jsonObject.getString("name_team");
            		String mdp_user = jsonObject.getString("mdp_user");
            		int id_team = Integer.parseInt(jsonObject.getString("id_team"));
            		int isAdmin = Integer.parseInt(jsonObject.getString("isAdmin"));
            		
            		User u = new User(id_user, nom_user, prenom_user, email_user, isAdmin, id_team, name_team, mdp_user);
            		
            		users.add(u);
            		
            	}

            } catch (Exception e){
            	e.printStackTrace();
            }
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return users;
		
	}
	
	public String addUser (int isAdmin, String mail_user, String mdp_user, String nom_user, String prenom_user, int id_team) {

		try {
			
			String dataUrl = "http://vps295091.ovh.net/php/addUser.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setDoOutput(true);
            
            OutputStream OS = httpUrlConnection.getOutputStream();
            
            BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = URLEncoder.encode("mail_user", "UTF-8") + "=" + URLEncoder.encode(mail_user,"UTF-8") + "&" +
            		URLEncoder.encode("isAdmin", "UTF-8") + "=" + URLEncoder.encode(String.valueOf(isAdmin),"UTF-8") + "&" +
                    URLEncoder.encode("mdp_user", "UTF-8") + "=" + URLEncoder.encode(mdp_user,"UTF-8") + "&" +
                    URLEncoder.encode("nom_user", "UTF-8") + "=" + URLEncoder.encode(nom_user,"UTF-8") + "&" +
                    URLEncoder.encode("prenom_user", "UTF-8") + "=" + URLEncoder.encode(prenom_user,"UTF-8") + "&" +
                    URLEncoder.encode("id_team", "UTF-8") + "=" + URLEncoder.encode(String.valueOf(id_team),"UTF-8");
            
            bufferwriter.write(data);
            bufferwriter.flush();
            bufferwriter.close();
            OS.close();
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res += line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();
            
            return res;
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
	
	public String updateUser(int isAdmin, Team newteam) {
		try {
			
			String dataUrl = "http://vps295091.ovh.net/php/updateUser.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            
            OutputStream OS = httpUrlConnection.getOutputStream();
            
            BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = URLEncoder.encode("isAdmin", "UTF-8") + "=" +URLEncoder.encode(String.valueOf(isAdmin),"UTF-8") + "&" +
                    URLEncoder.encode("description", "UTF-8") + "=" +URLEncoder.encode(String.valueOf(newteam.getId_team()),"UTF-8");
            
            bufferwriter.write(data);
            bufferwriter.flush();
            bufferwriter.close();
            OS.close();
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res += line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();

            return res;
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String deleteUser(int id_user) {
		try {
			
			String dataUrl = "http://vps295091.ovh.net/php/deleteUser.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            
            OutputStream OS = httpUrlConnection.getOutputStream();
            
            BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = URLEncoder.encode("id_user", "UTF-8") + "=" + URLEncoder.encode(Integer.toString(id_user),"UTF-8");
            
            bufferwriter.write(data);
            bufferwriter.flush();
            bufferwriter.close();
            OS.close();
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res+=line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();
            
            System.out.println(res);
            return res;
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
