package Services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

import Models.Project;


public class ProjectService {
	
	public ProjectService() {
		
	}
	
	
	public List<Project> getAllProjects() {
		
		List<Project> projects = new ArrayList<Project>();
		try {
			
			String dataUrl = "http://vps295091.ovh.net/php/Allprojects.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("GET");
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res+=line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();
            System.out.println(res);
      
            
            try {
            	JSONArray array =  new JSONArray(res);
            	
            	for (int i = 0; i < array.length(); i++) {
            		
            		JSONObject jsonObject = array.getJSONObject(i);
            		int id_projet = Integer.parseInt(jsonObject.getString("id_projet"));
            		String libelle_projet = jsonObject.getString("libelle_projet");
            		String lead_projet = jsonObject.getString("chef_projet");
            		String descripton_projet = jsonObject.getString("description_projet");
            		float avancement_projet = Float.parseFloat(jsonObject.getString("avancement_projet"));
            		
            		    
        		    Project proj = new Project(id_projet, libelle_projet, lead_projet, avancement_projet, descripton_projet);
        			projects.add(proj);

            	}

            } catch (Exception e){
            	e.printStackTrace();
            }
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return projects;
		
	}

	
	public String addProject (String libelle, String chefDeProjet, String description) {

		try {
			
			String dataUrl = "http://vps295091.ovh.net/php/addProject.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setDoOutput(true);
            
            OutputStream OS = httpUrlConnection.getOutputStream();
            
            BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = URLEncoder.encode("libelle", "UTF-8") + "=" +URLEncoder.encode(libelle,"UTF-8") + "&" +
                    URLEncoder.encode("chefDeProjet", "UTF-8") + "=" +URLEncoder.encode(chefDeProjet,"UTF-8") + "&" +
                    URLEncoder.encode("description", "UTF-8") + "=" +URLEncoder.encode(description,"UTF-8") + "&";
            
            bufferwriter.write(data);
            bufferwriter.flush();
            bufferwriter.close();
            OS.close();
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res+=line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();

            return res;
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
            
	
	}

	public String updateProject(int id_pro, String libelle_pro, String chefdeprojet_pro, String description_pro) {
		try {
			
			String dataUrl = "http://vps295091.ovh.net/php/updateProject.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            
            OutputStream OS = httpUrlConnection.getOutputStream();
            
            BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = URLEncoder.encode("id_pro", "UTF-8") + "=" + URLEncoder.encode(Integer.toString(id_pro),"UTF-8") + "&" +
                    URLEncoder.encode("libelle_pro", "UTF-8") + "=" + URLEncoder.encode(libelle_pro,"UTF-8") + "&" +
                    URLEncoder.encode("chefdeprojet_pro", "UTF-8") + "=" + URLEncoder.encode(chefdeprojet_pro,"UTF-8") + "&" +
                    URLEncoder.encode("description_pro", "UTF-8") + "=" + URLEncoder.encode(description_pro,"UTF-8") + "&";
            
            bufferwriter.write(data);
            bufferwriter.flush();
            bufferwriter.close();
            OS.close();
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res+=line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();

            return res;
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	
	public String deleteProject(int id_pro) {
		try {
			
			String dataUrl = "http://vps295091.ovh.net/php/DeleteProject.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            
            OutputStream OS = httpUrlConnection.getOutputStream();
            
            BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = URLEncoder.encode("id_pro", "UTF-8") + "=" + URLEncoder.encode(Integer.toString(id_pro),"UTF-8");
            
            bufferwriter.write(data);
            bufferwriter.flush();
            bufferwriter.close();
            OS.close();
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res += line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();

            return res;
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String deleteModulesByProject(int id_pro) {
		try {
			
			String dataUrl = "http://vps295091.ovh.net/php/deleteModule.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            
            OutputStream OS = httpUrlConnection.getOutputStream();
            
            BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = URLEncoder.encode("id_pro", "UTF-8") + "=" + URLEncoder.encode(Integer.toString(id_pro),"UTF-8");
            
            bufferwriter.write(data);
            bufferwriter.flush();
            bufferwriter.close();
            OS.close();
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res += line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();

            return res;
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
