package Services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import Models.Module;

public class ModuleService {

	public ModuleService() {
		// TODO Auto-generated constructor stub
	}

	public List<Module> getAllModules() {
		
		List<Module> modules = new ArrayList<Module>();
		try {
			
			String dataUrl = "http://vps295091.ovh.net/php/allModules.php";
			URL url = new URL(dataUrl);
			
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("GET");
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res += line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();
            
            try {
            	JSONArray array =  new JSONArray(res);
            	
            	for (int i = 0; i < array.length(); i++) {
            		
            		JSONObject jsonObject = array.getJSONObject(i);
            		
            		int id_module = Integer.parseInt(jsonObject.getString("id"));
            		String name_module = jsonObject.getString("name");
            		float progress_module = Float.parseFloat(jsonObject.getString("progress"));
            		String description_module = jsonObject.getString("description");
            		int id_projet = Integer.parseInt(jsonObject.getString("id_project"));
            		String libelle_projet = jsonObject.getString("name_project");

            		Module module = new Module (id_module, name_module, progress_module, description_module, id_projet, libelle_projet);
            		
            		modules.add(module);
            	}
            
            } catch (Exception e) {
            	e.printStackTrace();
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return modules;
		
	}

	
	public String addModule (String name_module, String description, double d, int id_module) {

		try {
			
			String dataUrl = "http://vps295091.ovh.net/php/addModule.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setDoOutput(true);
            
            OutputStream OS = httpUrlConnection.getOutputStream();
            
            BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = URLEncoder.encode("name_module", "UTF-8") + "=" + URLEncoder.encode(name_module,"UTF-8") + "&" +
                    URLEncoder.encode("Description_module", "UTF-8") + "=" + URLEncoder.encode(description,"UTF-8") + "&" +
                    URLEncoder.encode("progress_module", "UTF-8") + "=" + URLEncoder.encode(String.valueOf(d),"UTF-8") + "&" +
                    URLEncoder.encode("id_pro", "UTF-8") + "=" + URLEncoder.encode(String.valueOf(id_module),"UTF-8") + "&";
            
            bufferwriter.write(data);
            bufferwriter.flush();
            bufferwriter.close();
            OS.close();
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res += line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();
            
            System.out.println(res);
            
            return res;
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
            
	
	}

	public String updateModule(String name_module, String description, double progress, int id_module) {
		try {
			
			String dataUrl = "http://vps295091.ovh.net/php/updateModule.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            
            OutputStream OS = httpUrlConnection.getOutputStream();
            
            BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = URLEncoder.encode("name_module", "UTF-8") + "=" +URLEncoder.encode(name_module,"UTF-8") + "&" +
                    URLEncoder.encode("description", "UTF-8") + "=" +URLEncoder.encode(description,"UTF-8") + "&" +
                    URLEncoder.encode("progress_module", "UTF-8") + "=" +URLEncoder.encode(String.valueOf(progress),"UTF-8") + "&" +
                    URLEncoder.encode("id_module", "UTF-8") + "=" +URLEncoder.encode(String.valueOf(id_module),"UTF-8") + "&";
            
            bufferwriter.write(data);
            bufferwriter.flush();
            bufferwriter.close();
            OS.close();
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res+=line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();
            System.out.println(res);
            return res;
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	
	public String deleteModule(int id_module) {
		try {
			
			String dataUrl = "http://vps295091.ovh.net/php/deleteModule.php";
			URL url = new URL(dataUrl);
            HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            
            OutputStream OS = httpUrlConnection.getOutputStream();
            
            BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
            String data = URLEncoder.encode("id_module", "UTF-8") + "=" + URLEncoder.encode(Integer.toString(id_module),"UTF-8");
            
            bufferwriter.write(data);
            bufferwriter.flush();
            bufferwriter.close();
            OS.close();
            InputStream IN = httpUrlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
            String res = "";
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                res+=line;
            }
            bufferedReader.close();
            IN.close();
            httpUrlConnection.disconnect();
            
            return res;
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
