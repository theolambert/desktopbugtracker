package Models;

//Classe permettant de set des comboBox � l'aide d'une clef mais �galement d'une valeur 

public class KeyValuePair {
	
	  private int key;
	  private String value;
	  
	  public KeyValuePair(int i, String value) {
	    this.key = i;
	    this.value = value;
	  }

	  public int getKey() {
	    return key;
	  }
	  
	  public String getValue() {
	    return value;
	  }

	  public String toString() {
	    return value;
	  }
	}