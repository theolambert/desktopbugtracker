package Models;

public class Project {
	
	private int id_project;
	private String libelle_project;
	private String lead_project;
	private String description_project;
	private Float avancement_pro;
	
	public Project () {
		
	}

	public int getId_project() {
		return id_project;
	}

	public void setId_project(int id_project) {
		this.id_project = id_project;
	}

	public String getLibelle_project() {
		return libelle_project;
	}

	public void setLibelle_project(String libelle_project) {
		this.libelle_project = libelle_project;
	}

	public String getLead_project() {
		return lead_project;
	}

	public void setLead_project(String lead_project) {
		this.lead_project = lead_project;
	}

	public Project(String lib, String lead) {
		this.libelle_project = lib;
		this.lead_project = lead;
	}
	
	public Project(int id, String lib, String lead, Float avancement, String desc) {
		this.id_project = id;
		this.libelle_project = lib;
		this.lead_project = lead;
		this.avancement_pro = avancement;
		this.setDescription_project(desc);
	}

	public String getDescription_project() {
		return description_project;
	}

	public void setDescription_project(String description_project) {
		this.description_project = description_project;
	}

	public double getAvancement_pro() {
		return avancement_pro;
	}

	public void setAvancement_pro(Float avancement_pro) {
		this.avancement_pro = avancement_pro;
	}

}
