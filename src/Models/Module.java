package Models;

public class Module {
	
	private int id_module;
	private String name_module;
	private Float progress_module;
	private String description_module;
	private int id_pro;
	private String libelle_pro;
	
	public Module() {
		
	}

	public int getId_module() {
		return id_module;
	}

	public void setId_module(int id_module) {
		this.id_module = id_module;
	}

	public String getName_module() {
		return name_module;
	}

	public void setName_module(String name_module) {
		this.name_module = name_module;
	}

	public Float getProgress_module() {
		return progress_module;
	}

	public void setProgress_module(Float progress_module) {
		this.progress_module = progress_module;
	}

	public int getId_pro() {
		return id_pro;
	}

	public void setId_pro(int id_pro) {
		this.id_pro = id_pro;
	}
	
	public String getLibelle_pro() {
		return libelle_pro;
	}

	public void setLibelle_pro(String libelle_pro) {
		this.libelle_pro = libelle_pro;
	}

	public Module(int id, String name, Float progress, String description, int id_p, String lib_p) {
		this.id_module = id;
		this.name_module = name;
		this.progress_module = progress;
		this.description_module = description;
		this.id_pro = id_p;
		this.libelle_pro = lib_p;
	}

	public String getDescription_module() {
		return description_module;
	}

	public void setDescription_module(String description_module) {
		this.description_module = description_module;
	}
}
