package Models;


public class Incident {
	
	private int id_incident;
	private String libelle_inc;
	private String description_inc;
	private String statement;
	private String priority_inc;
	private int id_user;
	private String name_user;
	private int id_pro ;
	private String libelle_pro;
	private int id_module;
	private String name_module;
	private float progress;

	public Incident (int id_inc, String libelle, String description, String state, String prio, int id_user, String name_user, int id_pro, String libelle_pro, int id_module, String name_module, Float prog) {
		this.id_incident = id_inc;
		this.libelle_inc = libelle;
		this.description_inc = description;
		this.statement = state;
		this.priority_inc = prio;
		this.setId_user(id_user);
		this.setName_user(name_user);
		this.id_pro = id_pro;
		this.libelle_pro = libelle_pro;
		this.id_module = id_module;
		this.name_module = name_module;
		this.progress = prog;
	}
	
	public Incident() {
		// TODO Auto-generated constructor stub
	}

	public int getId_incident() {
		return id_incident;
	}

	public void setId_incident(int id_incident) {
		this.id_incident = id_incident;
	}

	public String getLibelle_inc() {
		return libelle_inc;
	}

	public void setLibelle_inc(String libelle_inc) {
		this.libelle_inc = libelle_inc;
	}

	public String getDescription_inc() {
		return description_inc;
	}

	public void setDescription_inc(String description_inc) {
		this.description_inc = description_inc;
	}

	public int getId_pro() {
		return id_pro;
	}

	public void setId_pro(int id_pro) {
		this.id_pro = id_pro;
	}

	public String getStatement() {
		return statement;
	}

	public void setStatement(String statement) {
		this.statement = statement;
	}

	public String getPriority_inc() {
		return priority_inc;
	}

	public void setPriotiry(String priority) {
		this.priority_inc = priority;
	}

	public String getLibelle_pro() {
		return libelle_pro;
	}

	public void setLibelle_pro(String libelle_pro) {
		this.libelle_pro = libelle_pro;
	}

	public float getProgress() {
		return progress;
	}

	public void setProgress(float progress) {
		this.progress = progress;
	}

	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}

	public String getName_user() {
		return name_user;
	}

	public void setName_user(String name_user) {
		this.name_user = name_user;
	}

	public int getId_module() {
		return id_module;
	}

	public void setId_module(int id_module) {
		this.id_module = id_module;
	}

	public String getName_module() {
		return name_module;
	}

	public void setName_module(String name_module) {
		this.name_module = name_module;
	}

}
