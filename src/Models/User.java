package Models;

public class User {
	
	private int id_user;
	private String nom_user;
	private String prenom_user;
	private String mail_user;
	private String mdp_user;
	private int isAdmin;
	private int id_team;
	private String name_team;
	
	
	public User () {
		
	}

	public int getId_user() {
		return id_user;
	}


	public void setId_user(int id_user) {
		this.id_user = id_user;
	}


	public String getNom_user() {
		return nom_user;
	}


	public void setNom_user(String nom_user) {
		this.nom_user = nom_user;
	}


	public String getPrenom_user() {
		return prenom_user;
	}


	public void setPrenom_user(String prenom_user) {
		this.prenom_user = prenom_user;
	}


	public String getMail_user() {
		return mail_user;
	}


	public void setMail_user(String mail_user) {
		this.mail_user = mail_user;
	}


	public String getMdp_user() {
		return mdp_user;
	}


	public void setMdp_user(String mdp_user) {
		this.mdp_user = mdp_user;
	}


	public User(int id, String nom, String prenom, String mail, String mdp, int admin, int id_tea, String lib_team) {
		this.id_user = id;
		this.nom_user = nom;
		this.prenom_user = prenom;
		this.mail_user = mail;
		this.mdp_user = mdp;
		this.name_team = lib_team;
		this.id_team = id_tea;
		this.isAdmin = admin;
	}
	
	public User(int id, String nom, String prenom, String mail, int admin, int id_tea, String lib_team, String mdp_user) {
		this.id_user = id;
		this.nom_user = nom;
		this.prenom_user = prenom;
		this.mail_user = mail;
		this.name_team = lib_team;
		this.id_team = id_tea;
		this.isAdmin = admin;
		this.mdp_user = mdp_user;
	}

	public int getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(int isAdmin) {
		this.isAdmin = isAdmin;
	}

	public int getId_team() {
		return id_team;
	}

	public void setId_team(int id_team) {
		this.id_team = id_team;
	}

	public String getName_team() {
		return name_team;
	}

	public void setName_team(String name_team) {
		this.name_team = name_team;
	}

}
