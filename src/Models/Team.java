package Models;

public class Team {
	
	private int id_team;
	private String name_team;

	public int getId_team() {
		return id_team;
	}

	public void setId_team(int id_team) {
		this.id_team = id_team;
	}

	public String getName_team() {
		return name_team;
	}

	public void setName_team(String name_team) {
		this.name_team = name_team;
	}

	public Team(int id, String name) {
		this.id_team = id;
		this.name_team = name;
	}

	public Team() {
		// TODO Auto-generated constructor stub
	}

}
