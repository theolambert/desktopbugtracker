package Controllers;

import java.io.IOException;

import application.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class HomePageController {

	@FXML
	Button btnUsers;
	
	@FXML
	Button btnProjects;
	
	@FXML 
	Button btnIncidents;
	
	@FXML 
	Button btnModules;
	
	@FXML 
	Button btnTeam;
	
	@FXML
	public void btnUsers_OnAction(ActionEvent event) throws IOException{

		try {
			Stage stage;
			Parent users_page_parent;
			
			stage = (Stage) btnUsers.getScene().getWindow();
			
			users_page_parent =  FXMLLoader.load(Main.class.getResource("/Views/UsersPage.fxml"));
			
			Scene users_page_scene = new Scene(users_page_parent);
			
			stage.setScene(users_page_scene);
			stage.show();

		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}
	
	@FXML
	public void btnIncidents_OnAction(ActionEvent event) throws IOException{

		try {
			Stage stage;
			Parent incidents_page_parent;
			
			stage = (Stage) btnIncidents.getScene().getWindow();
			
			incidents_page_parent =  FXMLLoader.load(Main.class.getResource("/Views/IncidentsPage.fxml"));
			
			Scene incidents_page_scene = new Scene(incidents_page_parent);
			
			stage.setScene(incidents_page_scene);
			stage.show();

		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}
	
	@FXML
	public void btnProjects_OnAction(ActionEvent event) throws IOException{

		try {
			Stage stage;
			Parent projects_page_parent;
			
			stage = (Stage) btnIncidents.getScene().getWindow();
			
			projects_page_parent =  FXMLLoader.load(Main.class.getResource("/Views/ProjectsPage.fxml"));
			
			Scene projects_page_scene = new Scene(projects_page_parent);
			
			stage.setScene(projects_page_scene);
			stage.show();

		} catch (IOException e) {

			e.printStackTrace();
		}
		
	}
	
	@FXML
	public void btnTeam_OnAction(ActionEvent event) throws IOException{

		try {
			Stage stage;
			Parent team_parent;
			
			stage = (Stage) btnTeam.getScene().getWindow();
			
			team_parent =  FXMLLoader.load(Main.class.getResource("/Views/TeamPage.fxml"));
			
			Scene team_page_scene = new Scene(team_parent);
			
			stage.setScene(team_page_scene);
			stage.show();

		} catch (IOException e) {

			e.printStackTrace();
		}
		
	}
	
	@FXML
	public void btnModules_OnAction(ActionEvent event) throws IOException{

		try {
			Stage stage;
			Parent modules_page_parent;
			
			stage = (Stage) btnModules.getScene().getWindow();
			
			modules_page_parent =  FXMLLoader.load(Main.class.getResource("/Views/ModulesPage.fxml"));
			
			Scene modules_page_scene = new Scene(modules_page_parent);
			
			stage.setScene(modules_page_scene);
			stage.show();

		} catch (IOException e) {

			e.printStackTrace();
		}
		
	}
}
