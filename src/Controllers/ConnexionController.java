package Controllers;

import java.io.IOException;

import Services.LoginService;
import application.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class ConnexionController {
	
	@FXML
	Button Button_login;
	
	@FXML
	Label Label_test;

	@FXML
	TextField TextField_username;

	@FXML
	PasswordField PasswordField_pwd;

	@FXML
	public void Button_login_OnAction(ActionEvent event) throws IOException{
		
		LoginService loginService = new LoginService();
		
		
		if (loginService.launchLoginService(TextField_username.getText().toString(), PasswordField_pwd.getText().toString(), "1")) {
			try {
				Stage stage;
				Parent home_page_parent;
				
				stage = (Stage) Button_login.getScene().getWindow();
				
				//Load the FXML file HomePage 
				home_page_parent =  FXMLLoader.load(Main.class.getResource("/Views/HomePage.fxml"));
				
				Scene home_page_scene = new Scene(home_page_parent);
				
				stage.setScene(home_page_scene);
				stage.show();

			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
		
	}
	
	

}
