package Controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import Models.Incident;
import Models.KeyValuePair;
import Models.Module;
import Models.Project;
import Models.User;
import Services.IncidentService;
import Services.ModuleService;
import Services.ProjectService;
import Services.UserService;
import application.Main;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class IncidentsController implements Initializable{

	@FXML
	TableView<Incident> tableViewIncidents;
	
	@FXML
	TableColumn <Incident, Integer> id_incident;
	
	@FXML
	TableColumn <Incident, String> libelle_inc;
	
	@FXML
	TableColumn <Incident, String> statement;
	
	@FXML
	TableColumn <Incident, String> name_user;
	
	@FXML
	TableColumn <Incident, String> priority_inc;
	
	@FXML
	TableColumn <Incident, String> libelle_pro;
	
	@FXML 
	TextField txtField_progress;
	
	@FXML
	TextField txtField_libincident;
	
	@FXML
	TextField filterInput_Libelle;
	
	@FXML
	TextField filterInput_Etat;
	
	@FXML
	TextField filterInput_Project;
	
	@FXML
	TextField filterInput_user;
	
	@FXML
	TextArea txtArea_Description;
	
	@FXML 
	ComboBox <String>comboBoxEtat;
	
	@FXML
	ComboBox<KeyValuePair> comboBoxProjects;
	
	@FXML
	ComboBox<KeyValuePair> comboBoxUserAssign;
	
	@FXML
	ComboBox<KeyValuePair> comboBoxUserReport;
	
	@FXML
	ComboBox<KeyValuePair> comboBoxModules;
	
	@FXML
	ComboBox<String> comboBoxPriority;
	
	@FXML
	Button btnAjouter;
	
	@FXML
	Button btnEditer;
	
	@FXML
	Button btnSupprimer;
	
	@FXML
	Button btn_Back;
	
	@FXML
	ProgressBar progressBarIncident;

	//Initialisations 
	int lastID = 0;
	ProjectService projectService = new ProjectService();
	IncidentService incidentService = new IncidentService();
	UserService userService = new UserService();
	ModuleService moduleService = new ModuleService();
	
	List<Incident> incidents = incidentService.getAllIncidents();
	List <Project> projects = projectService.getAllProjects();
	List<User> users = userService.getAllUsers();
	List<Module> modules = moduleService.getAllModules();
	
	final ObservableList<Incident> items = FXCollections.observableArrayList(incidents);
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		//Chargement des donn�es dans les diff�rentes ComboBox
		loadComboBoxProjects();
		loadComboBoxUser();
		loadcomboBoxEtat();
		loadcomboBoxPriority();
		loadComboBoxModules();
		
		//add Listener to filterField
		filterInput_Libelle.textProperty().addListener(new ChangeListener<Object>() {
			public void changed(ObservableValue<?> observable, Object oldValue, Object newValue) {
				filterIncidentsByName((String) oldValue, (String) newValue);
			}
		});
		
		//add Listener to filterField
		filterInput_Etat.textProperty().addListener(new ChangeListener<Object>() {
			public void changed(ObservableValue<?> observable, Object oldValue, Object newValue) {
				FilterIncidentListByStatement((String) oldValue, (String) newValue);
			}
		});
		
		//add Listener to filterField
		filterInput_Project.textProperty().addListener(new ChangeListener<Object>() {
			public void changed(ObservableValue<?> observable, Object oldValue, Object newValue) {
				filterIncidentListByProject((String) oldValue, (String) newValue);
			}
		});
		
		//add Listener to filterField
		filterInput_user.textProperty().addListener(new ChangeListener<Object>() {
			public void changed(ObservableValue<?> observable, Object oldValue, Object newValue) {
				filterIncidentListByUser((String) oldValue, (String) newValue);
			}
		});
		
		id_incident.setCellValueFactory(
		    new PropertyValueFactory<>("id_incident")
		);
		
        libelle_inc.setCellValueFactory(
		    new PropertyValueFactory<>("libelle_inc")
		);
        
        statement.setCellValueFactory(
		    new PropertyValueFactory<>("statement")
		);
        
        name_user.setCellValueFactory(
		    new PropertyValueFactory<>("name_user")
		);
        
        priority_inc.setCellValueFactory(
		    new PropertyValueFactory<>("priority_inc")
		);
        
        libelle_pro.setCellValueFactory(
		    new PropertyValueFactory<>("libelle_pro")
		);

        //Si la liste des incidents n'est pas nulle, alors on peut Set les value de la tableView
        if (incidents != null) {
        	tableViewIncidents.setItems(items);
    		
    		lastID = items.get(items.size()-1).getId_incident();
        }
	}

	//Fonction qui permet de charger la ComboBoxPriority avec les diff�rentes valeurs donn�es ci-dessous
	public void loadcomboBoxPriority() {
		new Thread(){
			public void run() {
				List<String> priorities = new ArrayList<String>();
				priorities.add("Blocker");
				priorities.add("Critical");
				priorities.add("Major");
				priorities.add("Minor");
				priorities.add("Trivial");
				
				for (String str : priorities) {
					Platform.runLater(() -> comboBoxPriority.getItems().add(str));
				}
				
				 try {
				     Thread.sleep(1000); 
				 } catch(InterruptedException ex) {
				     Thread.currentThread().interrupt();
				 } 
             }
         }.start();
	}
	
	//Fonction qui permet de charger la comboBoxProject avec une classe KeyValuePair afin d'obtenir l'id et le nom
	public void loadComboBoxProjects() {
		
		//Nouveau thread afin de ne pas bloquer le thread UI principal
		new Thread(){
			public void run() {
				if (projects != null) {
					for (Project p : projects) {
						Platform.runLater(() -> comboBoxProjects.getItems().add(new KeyValuePair(p.getId_project(), p.getLibelle_project())));
						 
					}
				}
				
                 try {
                     Thread.sleep(1000); 
                 } catch(InterruptedException ex) {
                     Thread.currentThread().interrupt();
                 } 
             }
         }.start();
	}
	
	//Chargement des donn�es pour la comboBox Modules
	public void loadComboBoxModules() {

		new Thread(){
			public void run() {
				if (modules != null) {
					for (Module m : modules) { 
						Platform.runLater(() -> comboBoxModules.getItems().add(new KeyValuePair(m.getId_module(), m.getName_module())));
					}
				}
				

                 try {
                     Thread.sleep(1000); 
                 } catch(InterruptedException ex) {
                     Thread.currentThread().interrupt();
                 } 
             }
         }.start();
	}
	
	//Chargement des donn�es pour la ComboBoxUserAssign et comboBoxUserReport
	public void loadComboBoxUser() {

		new Thread(){
			public void run() {
				if (users != null) {
					for (User u : users) { 
						Platform.runLater(() -> comboBoxUserAssign.getItems().add(new KeyValuePair(u.getId_user(), u.getPrenom_user() + u.getNom_user())));
						Platform.runLater(() -> comboBoxUserReport.getItems().add(new KeyValuePair(u.getId_user(), u.getPrenom_user() + u.getNom_user())));
					}
				}
				
                 try {
                     Thread.sleep(1000); 
                 } catch(InterruptedException ex) {
                     Thread.currentThread().interrupt();
                 } 
             }
         }.start();
	}
	
	//Chargement des doon�es pour la comboBoxEtat
	public void loadcomboBoxEtat() {
		new Thread(){
			public void run() {
				List<String> etats = new ArrayList<String>();
				etats.add("New");
				etats.add("Accept");
				etats.add("Resolved");
				etats.add("Closed");
				
				for (String str : etats) {
					Platform.runLater(() -> comboBoxEtat.getItems().add(str));
				}
				
				 try {
				     Thread.sleep(1000); 
				 } catch(InterruptedException ex) {
				     Thread.currentThread().interrupt();
				 } 
             }
         }.start();
	}
	
	//Filter the table of Incidents by the projectName Value set in the comboBoxFilterProject
    public void filterIncidentListByProject(String oldValue, String newValue) {
        ObservableList<Incident> filteredList = FXCollections.observableArrayList();
        if(filterInput_Project == null || (newValue.length() < oldValue.length()) || newValue == null) {
        	tableViewIncidents.setItems(items);
        }
        else {
            newValue = newValue.toUpperCase();
            for(Incident inc : tableViewIncidents.getItems()) {
                String filterNameProject = inc.getLibelle_pro();
                
                if(filterNameProject.toUpperCase().contains(newValue)) {
                    filteredList.add(inc);
                }
            }
            tableViewIncidents.setItems(filteredList);
        }
    }
    
  //Filter the table of Incidents by the user assign
    public void filterIncidentListByUser(String oldValue, String newValue) {
        ObservableList<Incident> filteredList = FXCollections.observableArrayList();
        if(filterInput_Project == null || (newValue.length() < oldValue.length()) || newValue == null) {
        	tableViewIncidents.setItems(items);
        }
        else {
            newValue = newValue.toUpperCase();
            for(Incident inc : tableViewIncidents.getItems()) {
                String filterNameUser = inc.getName_user();
                
                if(filterNameUser.toUpperCase().contains(newValue)) {
                    filteredList.add(inc);
                }
            }
            tableViewIncidents.setItems(filteredList);
        }
    }
    
  //Filter the table of Incidents by the statement set in the comboBox
    public void FilterIncidentListByStatement(String oldValue, String newValue) {
        ObservableList<Incident> filteredList = FXCollections.observableArrayList();
        if(filterInput_Etat == null || (newValue.length() < oldValue.length()) || newValue == null) {
        	tableViewIncidents.setItems(items);
        }
        else {
            newValue = newValue.toUpperCase();
            for(Incident inc : tableViewIncidents.getItems()) {
                String filterStatement = inc.getStatement();
                
                if(filterStatement.toUpperCase().contains(newValue)) {
                    filteredList.add(inc);
                }
            }
            tableViewIncidents.setItems(filteredList);
        }
    }
	
  //Filter the table View By the name of the incident
    public void filterIncidentsByName(String oldValue, String newValue) {
        ObservableList<Incident> filteredList = FXCollections.observableArrayList();
        if(filterInput_Libelle == null || (newValue.length() < oldValue.length()) || newValue == null) {
        	tableViewIncidents.setItems(items);
        }
        else {
            newValue = newValue.toUpperCase();
            for(Incident inc : tableViewIncidents.getItems()) {
                String filterNameIncident = inc.getLibelle_inc();
                
                if(filterNameIncident.toUpperCase().contains(newValue)) {
                    filteredList.add(inc);
                }
            }
            tableViewIncidents.setItems(filteredList);
        }
    }
	
    public void btnAjouter_OnAction(ActionEvent event) {
		
    	//V�rifs champs non vides
		if (!(comboBoxModules.getSelectionModel().selectedItemProperty().get().getValue().isEmpty() || comboBoxProjects.getSelectionModel().selectedItemProperty().get().getValue().isEmpty() || comboBoxUserAssign.getSelectionModel().selectedItemProperty().get().getValue().isEmpty() || txtArea_Description.getText().isEmpty() || txtField_libincident.getText().isEmpty())) {

			Incident inc = new Incident();
			float prog;
			
			//Si le textField contenant la progression n'est pas rempli alors valeur par d�fault 0 
			if (txtField_progress.getText().isEmpty()) {
				prog = 0;
			} else {
				prog = Float.parseFloat(txtField_progress.getText());
			}
			
			if (lastID == items.get(items.size()-1).getId_incident()) {
				inc = new Incident (
						lastID + 1,
						txtField_libincident.getText(),
						txtArea_Description.getText(),
						comboBoxEtat.getValue(),
						comboBoxPriority.getValue(),
						comboBoxUserAssign.getSelectionModel().selectedItemProperty().get().getKey(),
						comboBoxUserAssign.getSelectionModel().selectedItemProperty().get().getValue(),
						comboBoxProjects.getSelectionModel().selectedItemProperty().get().getKey(),
						comboBoxProjects.getSelectionModel().selectedItemProperty().get().getValue(),
						comboBoxModules.getSelectionModel().selectedItemProperty().get().getKey(),
						comboBoxModules.getSelectionModel().selectedItemProperty().get().getValue(),
						prog
					);
				
			} else {
				inc = new Incident (
						lastID,
						txtField_libincident.getText(),
						txtArea_Description.getText(),
						comboBoxEtat.getValue(),
						comboBoxPriority.getValue(),
						comboBoxUserAssign.getSelectionModel().selectedItemProperty().get().getKey(),
						comboBoxUserAssign.getSelectionModel().selectedItemProperty().get().getValue(),
						comboBoxProjects.getSelectionModel().selectedItemProperty().get().getKey(),
						comboBoxProjects.getSelectionModel().selectedItemProperty().get().getValue(),
						comboBoxModules.getSelectionModel().selectedItemProperty().get().getKey(),
						comboBoxModules.getSelectionModel().selectedItemProperty().get().getValue(),
						prog
					);
			}

			//Ajout dans l'obsevable List pour l'ajouter directement dans la tableView
			items.add(inc);
			
			//Appel au webService permettant d'ajouter un incident
			incidentService.addIncident(txtField_libincident.getText(), txtArea_Description.getText(), comboBoxEtat.getValue(), comboBoxUserReport.getSelectionModel().selectedItemProperty().get().getKey(), comboBoxUserAssign.getSelectionModel().selectedItemProperty().get().getKey(), comboBoxProjects.getSelectionModel().selectedItemProperty().get().getKey(), comboBoxModules.getSelectionModel().selectedItemProperty().get().getKey(), comboBoxPriority.getValue(), prog);
			
			
			txtArea_Description.setText("");
			txtField_progress.setText("");
			txtField_libincident.setText("");
			comboBoxEtat.setValue("Etat");
			comboBoxPriority.setValue("Priorit�");
			comboBoxUserAssign.setValue(new KeyValuePair(0, "Utilisateur"));
			comboBoxUserReport.setValue(new KeyValuePair(0, "Utilisateur"));
			comboBoxProjects.setValue(new KeyValuePair (0, "Projet"));
			comboBoxModules.setValue(new KeyValuePair (0, "Module"));
		}
	}

	public void btnEditer_OnAction(ActionEvent event) {
		
		//R�cup�ration de l'incident s�lectionn� dans la tableView
		Incident incidentSelected = tableViewIncidents.getSelectionModel().selectedItemProperty().get();
		
		//V�rifications champs non vides !
		if (!comboBoxProjects.getSelectionModel().selectedItemProperty().get().getValue().isEmpty() && !comboBoxUserAssign.getSelectionModel().selectedItemProperty().get().getValue().isEmpty() && !txtArea_Description.getText().isEmpty() && !txtField_libincident.getText().isEmpty() && !txtField_progress.getText().isEmpty()) {
			
			//Update de tous les champs de l'incident s�lectionn� de la tableView
			incidentSelected.setDescription_inc(txtArea_Description.getText());
			incidentSelected.setProgress(Float.parseFloat(txtField_progress.getText()));
			incidentSelected.setLibelle_inc(txtField_libincident.getText());
			incidentSelected.setStatement(comboBoxEtat.getValue());
			incidentSelected.setId_user(comboBoxUserAssign.getSelectionModel().selectedItemProperty().get().getKey());
			incidentSelected.setName_user(comboBoxUserAssign.getSelectionModel().selectedItemProperty().get().getValue());
			incidentSelected.setId_pro(comboBoxProjects.getSelectionModel().selectedItemProperty().get().getKey());
			incidentSelected.setLibelle_pro(comboBoxProjects.getSelectionModel().selectedItemProperty().get().getValue());
			
			//Appel au WebService permettant l'update
			incidentService.updateIncident(incidentSelected.getId_incident(), txtField_libincident.getText(), txtArea_Description.getText(), comboBoxEtat.getValue(), comboBoxUserAssign.getSelectionModel().selectedItemProperty().get().getKey(), Float.parseFloat(txtField_progress.getText()));
			
			//Refresh des champs
			txtArea_Description.setText("");
			txtField_progress.setText("");
			txtField_libincident.setText("");
			comboBoxEtat.setValue("Etat");
			comboBoxPriority.setValue("Priorit�");
			comboBoxUserAssign.setValue(new KeyValuePair(0, "Utilisateur"));
			comboBoxUserReport.setValue(new KeyValuePair(0, "Utilisateur"));
			comboBoxProjects.setValue(new KeyValuePair (0, "Projet"));
			comboBoxModules.setValue(new KeyValuePair (0, "Module"));
			
			//Refresh de la tableView de fa�on � pouvoir faire l'update de fa�on dynamique
			tableViewIncidents.setItems(items);
			tableViewIncidents.refresh();
		}
	}
    
    public void btnSupprimer_OnAction(ActionEvent event) {
    	//R�cup�ration de l'incide de l'item s�lectionn� et l'item en question
    	Incident incidentSelected = tableViewIncidents.getSelectionModel().selectedItemProperty().get();;
		int indexRowSelected = tableViewIncidents.getSelectionModel().selectedIndexProperty().get();
		
		lastID = items.get(items.size()-1).getId_incident() + 1;
		
		//Delete de l'ObservableList qui aura directement l'impact sur la tableView
		items.remove(indexRowSelected);
		
		//Delete de la Base de donn�es directement en appelant le web service
		incidentService.deleteIncident(incidentSelected.getId_incident());
	
		//Refresh des champs 
		txtArea_Description.setText("");
		txtField_progress.setText("");
		txtField_libincident.setText("");
		comboBoxEtat.setValue("Etat");
		comboBoxPriority.setValue("Priorit�");
		comboBoxUserAssign.setValue(new KeyValuePair(0, "Utilisateur"));
		comboBoxUserReport.setValue(new KeyValuePair(0, "Utilisateur"));
		comboBoxProjects.setValue(new KeyValuePair (0, "Projet"));
		comboBoxModules.setValue(new KeyValuePair (0, "Module"));
    }
    
	public void tableViewIncidents_OnMouseClicked (MouseEvent mouseevent){
		
		//Lors du click sur une ligne de la tableView on met � jour les champs avec les valeurs de l'item s�lectionn�
		Incident incidentSelected = tableViewIncidents.getSelectionModel().selectedItemProperty().get();
		
		txtArea_Description.setText(incidentSelected.getDescription_inc());
		txtField_progress.setText(String.valueOf(incidentSelected.getProgress()));
		txtField_libincident.setText(incidentSelected.getLibelle_inc());
		comboBoxEtat.setValue(incidentSelected.getStatement());
		comboBoxPriority.setValue(incidentSelected.getPriority_inc());
		comboBoxUserAssign.setValue(new KeyValuePair(incidentSelected.getId_user(), incidentSelected.getName_user()));
		comboBoxUserReport.setValue(new KeyValuePair(0, "Utilisateur"));
		comboBoxProjects.setValue(new KeyValuePair (incidentSelected.getId_pro(), incidentSelected.getLibelle_pro()));
		comboBoxModules.setValue(new KeyValuePair (incidentSelected.getId_module(), incidentSelected.getName_module()));
		
		new Thread(){
			public void run() {
            	 //New Thread car bloquant le thread principal sinon 
                 Platform.runLater(() -> progressBarIncident.setProgress(incidentSelected.getProgress()/100));
                 
                 try {
                     Thread.sleep(1000); 
                 } catch(InterruptedException ex) {
                     Thread.currentThread().interrupt();
                 } 
             }
         }.start();
		
	}

	//Bouton permettant de rammener vers la HomePage
	
	public void btn_Back_OnAction(ActionEvent event) {
		try {
			Stage stage;
			Parent home_page_parent;
			
			stage = (Stage) btn_Back.getScene().getWindow();
			
			home_page_parent =  FXMLLoader.load(Main.class.getResource("/Views/HomePage.fxml"));
			
			Scene home_page_scene = new Scene(home_page_parent);
			
			stage.setScene(home_page_scene);
			stage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
}
