package Controllers;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import Models.Team;
import Services.TeamService;
import application.Main;

public class TeamController implements Initializable{

	@FXML
	TextField txtField_nameTeam;
	
	@FXML 
	TextField filterInput;
	
	@FXML
	Button btnAjouter;
	
	@FXML
	Button btnEditer;
	
	@FXML
	Button btnSupprimer;
	
	@FXML
	TableView<Team> tableViewTeam;
	
	@FXML
	TableColumn<Team, Integer> id_team;
	
	@FXML
	TableColumn<Team, String> name_team;
	
	@FXML
	Button btn_Back;
	
	@FXML
	Label label_saisieName;
	
	int lastID = 0;
	
	TeamService teamService = new TeamService();
	
	List<Team> teams = teamService.getAllTeams();
	
	final ObservableList<Team> items = FXCollections.observableArrayList(teams);
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		//add Listener to filterField
        filterInput.textProperty().addListener(new ChangeListener<Object>() {
        public void changed(ObservableValue<?> observable, Object oldValue, Object newValue) {
        	filterTeamList((String) oldValue, (String) newValue);
	        }
        });
		
        id_team.setCellValueFactory(
		    new PropertyValueFactory<>("id_team")
		);
		
        name_team.setCellValueFactory(
		    new PropertyValueFactory<>("name_team")
		);
		
        //Si liste est nulle, on n'affecte pas les valeurs � la tableView
        if (teams != null) {
        	tableViewTeam.setItems(items);
    		
            lastID = items.get(items.size()-1).getId_team();
        }
	}
	
	//filter table by name or lead project name
    public void filterTeamList(String oldValue, String newValue) {
        ObservableList<Team> filteredList = FXCollections.observableArrayList();
        if(filterInput == null || (newValue.length() < oldValue.length()) || newValue == null) {
        	tableViewTeam.setItems(items);
        }
        else {
            newValue = newValue.toUpperCase();
            for(Team team : tableViewTeam.getItems()) {
                String filterNameTeam = team.getName_team();
                String filterIdTeam = String.valueOf(team.getId_team());
                
                if(filterNameTeam.toUpperCase().contains(newValue) || filterIdTeam.toUpperCase().contains(newValue)) {
                    filteredList.add(team);
                }
            }
            tableViewTeam.setItems(filteredList);
        }
    }

    public void btnAjouter_OnAction(ActionEvent event) {
		
		if (txtField_nameTeam.getText().isEmpty()) {
			label_saisieName.setText("Champ obligatoire");
		} else {
			Team team = new Team();
			
			if (lastID == items.get(items.size()-1).getId_team()) {
				team = new Team (
						lastID +1,
						txtField_nameTeam.getText()
				);
			} else {
				team = new Team (
						lastID,
						txtField_nameTeam.getText()
				);
			}
	
			//Ajout de l'item avec les champs mapp�s dans l'observableList
			items.add(team);
			
			//Appel du WebService 
			teamService.addTeam(txtField_nameTeam.getText());
			
			//REFRESH DES CHAMPS
			txtField_nameTeam.setText("");
		}

	}
    
    
	public void btnSupprimer_OnAction(ActionEvent event) {
		
		//R�cup�ration de l'indice et de l'item s�lectionn� sur la tableView
		
		Team teamSelected =  tableViewTeam.getSelectionModel().selectedItemProperty().get();
		int indexRowSelected = tableViewTeam.getSelectionModel().selectedIndexProperty().get();
		
		lastID = items.get(items.size()-1).getId_team() +1;
		
		//Suppression de l'�l�ment dans l'ObservableList -> TableView
		items.remove(indexRowSelected);
		
		//Appel du WebService 
		teamService.deleteTeam(teamSelected.getId_team());
		
		//REFRESH DES CHAMPS
		txtField_nameTeam.setText("");
	}
	
	public void tableViewTeams_OnMouseClicked (MouseEvent mouseevent){
		
		//Lors du click sur la tableView on mappe les champs associ�s de l'item s�lectionn�
		
		Team teamSelected =  tableViewTeam.getSelectionModel().selectedItemProperty().get();
		
		txtField_nameTeam.setText(teamSelected.getName_team());
		
     }

	
	public void btnEditer_OnAction(ActionEvent event) {
		
		//R�cup�ration item s�lectionn�
		Team teamSelected =  tableViewTeam.getSelectionModel().selectedItemProperty().get();
		
		//Test champ non vide 
		if (!txtField_nameTeam.getText().isEmpty()) {
			
			//Mise � jour des donn�es de l'objet s�lectionn�
			teamSelected.setName_team(txtField_nameTeam.getText());
			
			//Appel du webService avec les champs saisis par l'utilisateur 
			teamService.updateTeam(teamSelected.getId_team(), txtField_nameTeam.getText());
			
			//REFRESH DE LA TABLEVIEW avec l'objet update
			tableViewTeam.setItems(items);
			tableViewTeam.refresh();
			
			txtField_nameTeam.setText("");
		}
	}
	
	//Bouton retour vers la home page
	public void btn_Back_OnAction(ActionEvent event) {
		try {
			Stage stage;
			Parent home_page_parent;
			
			stage = (Stage) btn_Back.getScene().getWindow();
			
			home_page_parent =  FXMLLoader.load(Main.class.getResource("/Views/HomePage.fxml"));
			
			Scene home_page_scene = new Scene(home_page_parent);
			
			stage.setScene(home_page_scene);
			stage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
