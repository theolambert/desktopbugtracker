package Controllers;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import Services.ProjectService;
import application.Main;
import Models.Project;

public class ProjectsController implements Initializable {
	
	ProjectService projectService = new ProjectService();
	
	int lastID = 0;
	
	@FXML
	private Button btn_Back;
	
	@FXML
	private ProgressBar progress_projet;
	
	@FXML
	private TableView<Project> tableViewProjects;
	
	@FXML
	private TableColumn<Project, Integer> id_project;
	
	@FXML
	private TableColumn<Project, String> libelle_project;
	
	@FXML
	private TableColumn<Project, String> lead_project;
	
	@FXML
	private Button btnAjouter;
	
	@FXML
	private Button btnEditer;
	
	@FXML
	private Button btnSupprimer;
	
	@FXML
	private TextField txtField_Libelle;
	
	@FXML
	private TextField txtField_ChefDeProjet;
	
	@FXML
	private TextArea txtArea_Description;
	
	@FXML
	private Label label_saisieLibelle;
	
	@FXML
	private Label label_saisieChefDeProjet;
	
	@FXML
	private Label label_saisieDescription;
	
	@FXML
	private Label labelLASTID;
	
	@FXML
    private TextField filterInput;
	
	@FXML 
	private TextField txtField_progress;
	
	List<Project> projectsAsList = projectService.getAllProjects();
	
	final ObservableList<Project> items = FXCollections.observableArrayList(projectsAsList);
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		//add Listener to filterField
        filterInput.textProperty().addListener(new ChangeListener<Object>() {
            public void changed(ObservableValue<?> observable, Object oldValue, Object newValue) {
            	filterProjectList((String) oldValue, (String) newValue);

            }
        });
		
		id_project.setCellValueFactory(
		    new PropertyValueFactory<>("id_project")
		);
		
		libelle_project.setCellValueFactory(
		    new PropertyValueFactory<>("libelle_project")
		);
		lead_project.setCellValueFactory(
		    new PropertyValueFactory<>("lead_project")
		);

		//Si la liste est nulle, alors on n'affecte pas les donn�es � la tableView
		if (items != null) {
			tableViewProjects.setItems(items);

			lastID = items.get(items.size()-1).getId_project();
		}
		
		
	}
        
    //filter table by name or lead project name
    public void filterProjectList(String oldValue, String newValue) {
        ObservableList<Project> filteredList = FXCollections.observableArrayList();
        if(filterInput == null || (newValue.length() < oldValue.length()) || newValue == null) {
        	tableViewProjects.setItems(items);
        }
        else {
            newValue = newValue.toUpperCase();
            for(Project proj : tableViewProjects.getItems()) {
                String filterLibelleProject = proj.getLibelle_project();
                String filterLeadProject = proj.getLead_project();
                String filterIdProject = String.valueOf(proj.getId_project());
                
                if(filterLibelleProject.toUpperCase().contains(newValue) || filterIdProject.toUpperCase().contains(newValue) || filterLeadProject.toUpperCase().contains(newValue)) {
                    filteredList.add(proj);
                }
            }
            tableViewProjects.setItems(filteredList);
        }
    }
	
    //Sur click du bouton Ajouter :
	public void btnAjouter_OnAction(ActionEvent event) {
		
		if (txtField_Libelle.getText().isEmpty()) {
			label_saisieLibelle.setText("Champ obligatoire");
		}
		if (txtField_ChefDeProjet.getText().isEmpty()) {
			label_saisieChefDeProjet.setText("Champ obligatoire");
		}
		if (txtArea_Description.getText().isEmpty()) {
			label_saisieDescription.setText("Champ obligatoire");
		}
		
		//V�rification des champs non vides !
		if (!(txtArea_Description.getText().isEmpty() || txtField_ChefDeProjet.getText().isEmpty() || txtField_Libelle.getText().isEmpty())) {

			Project proj = new Project();
			float prog;
			
			//Si champ vide alors valeur par d�fault
			if (txtField_progress.getText().isEmpty()) {
				
				prog = 0;
			} else {
				prog = Float.parseFloat(txtField_progress.getText());
			}
			
			if (lastID == items.get(items.size()-1).getId_project()) {
				proj = new Project (
						lastID +1,
						txtField_Libelle.getText(),
						txtField_ChefDeProjet.getText(),
						prog,
						txtArea_Description.getText()
				);
			} else {
				proj = new Project (
						lastID,
						txtField_Libelle.getText(),
						txtField_ChefDeProjet.getText(),
						prog,
						txtArea_Description.getText()
				);
			}
			
			//Ajout dans l'observable List -> TableView
			items.add(proj);
			
			//Ajout dans la base de donn�es en appelant le web service
			projectService.addProject(txtField_Libelle.getText(), txtField_ChefDeProjet.getText(),txtArea_Description.getText());
			
			//REFRESH DES CHAMPS
			label_saisieLibelle.setText("");
			label_saisieChefDeProjet.setText("");
			label_saisieDescription.setText("");
			txtField_Libelle.setText("");
			txtField_ChefDeProjet.setText("");
			txtArea_Description.setText("");
			
			
		}
	}

	//On click sur le bouton supprimer
	public void btnSupprimer_OnAction(ActionEvent event) {
		
		//R�cup�ration de l'objet s�lectionn� et de l'indice associ� dans l'observableList
		Project projectSelected =  tableViewProjects.getSelectionModel().selectedItemProperty().get();
		int indexRowSelected = tableViewProjects.getSelectionModel().selectedIndexProperty().get();
		
		lastID = items.get(items.size()-1).getId_project() +1;
		
		//Suppression de l'�l�ment dans l'observable list et donc de la tableview
		items.remove(indexRowSelected);
		
		//Suppression du projet et des modules associ�s 
		projectService.deleteProject(projectSelected.getId_project());
		projectService.deleteModulesByProject(projectSelected.getId_project());
		
		//REFRESH DES CHAMPS
		txtField_Libelle.setText("");
		txtField_ChefDeProjet.setText("");
		txtArea_Description.setText("");

	}
	
	//Sur click d'une ligne de la tableView
	public void tableViewProjects_OnMouseCliked (MouseEvent mouseevent){
		
		//R�cup�ration de l'objet s�lectionn�
		Project projectSelected =  tableViewProjects.getSelectionModel().selectedItemProperty().get();
		
		//Mapping des champs avec ceux de l'objet s�lectionn�
		txtField_Libelle.setText(projectSelected.getLibelle_project());
		txtField_ChefDeProjet.setText(projectSelected.getLead_project());
		txtArea_Description.setText(projectSelected.getDescription_project());
		txtField_progress.setText(String.valueOf(projectSelected.getAvancement_pro()));
		
		new Thread(){
			public void run() {
            	 
                 Platform.runLater(() -> progress_projet.setProgress(projectSelected.getAvancement_pro()/100));
                 
                 try {
                     Thread.sleep(1000); 
                 } catch(InterruptedException ex) {
                     Thread.currentThread().interrupt();
                 } 
             }
         }.start();
     }

	
	public void btnEditer_OnAction(ActionEvent event) {
		
		//R�cup�ration de l'objet s�lectionn� dans la tableView
		Project proj = items.get(tableViewProjects.getSelectionModel().selectedIndexProperty().get());
		
		//V�rification champs non vides :
		if (!txtField_ChefDeProjet.getText().isEmpty() && !txtField_ChefDeProjet.getText().isEmpty() && !txtArea_Description.getText().isEmpty()) {
			proj.setLead_project(txtField_ChefDeProjet.getText());
			proj.setLibelle_project(txtField_Libelle.getText());
			proj.setDescription_project(txtArea_Description.getText());
			
			//Appel du webservice et update dans la base de donn�es
			projectService.updateProject(proj.getId_project(), txtField_Libelle.getText(), txtField_ChefDeProjet.getText(), txtArea_Description.getText());
			
			txtField_Libelle.setText("");
			txtField_ChefDeProjet.setText("");
			txtArea_Description.setText("");
			
			//REFRESH de la tableView
			tableViewProjects.setItems(items);
			tableViewProjects.refresh();
		}
	}
	
	//Bouton retour de la home_page
	public void btn_Back_OnAction(ActionEvent event) {
		try {
			Stage stage;
			Parent home_page_parent;
			
			stage = (Stage) btn_Back.getScene().getWindow();
			
			home_page_parent =  FXMLLoader.load(Main.class.getResource("/Views/HomePage.fxml"));
			
			Scene home_page_scene = new Scene(home_page_parent);
			
			stage.setScene(home_page_scene);
			stage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
