package Controllers;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import Models.KeyValuePair;
import Models.Team;
import Models.User;
import Services.TeamService;
import Services.UserService;
import application.Main;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class UsersController implements Initializable{

	UserService userService = new UserService();
	TeamService teamService = new TeamService();
	
	@FXML
	private TableView<User> tableViewUsers;
	
	@FXML
	private TableColumn<User, Integer> id_user;
	
	@FXML
	private TableColumn<User, String> nom_user;
	
	@FXML
	private TableColumn<User, String> prenom_user;
	
	@FXML
	private TableColumn<User, String> mail_user;
	
	@FXML
	private TableColumn<User, String> isAdmin;
	
	@FXML
	private TableColumn<User, String> name_team;
	
	@FXML
	TextField txtField_prenom;
	
	@FXML
	TextField txtField_mail;
	
	@FXML
	PasswordField PasswordFieldmdp;
	
	@FXML
	TextField filterInput;
	
	@FXML 
	TextField filterInput_team;

	@FXML
	TextField txtField_nom;
	
	@FXML
	Button btn_Back;
	
	@FXML
	Button btnAjouter;
	
	@FXML
	Button btnEditer;
	
	@FXML
	Button btnSupprimer;
	
	@FXML
	Label label_saisiePrenom;
	
	@FXML
	Label label_saisieNom;
	
	@FXML
	Label label_saisiemdp;
	
	@FXML
	Label label_saisieEmail;

	@FXML
	ComboBox<KeyValuePair> comboBoxisAdmin;
	
	@FXML
	ComboBox<KeyValuePair> comboBoxteam;
	

	List<User> usersAsList = userService.getAllUsers();
	final ObservableList<User> items = FXCollections.observableArrayList(usersAsList);
	
	List<Team> teams = teamService.getAllTeams();
	List<String> teamsNames = teamService.getAllNameTeams(teams);
	ObservableList<String> itemsTeam = FXCollections.observableArrayList(teamsNames);
	
	int lastID = 0;
		
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		loadComboBoxisAdmin();
		loadComboBoxTeam();
		
		//add Listener to filterField
		filterInput.textProperty().addListener(new ChangeListener<Object>() {
            public void changed(ObservableValue<?> observable, Object oldValue, Object newValue) {
            	filterUserList((String) oldValue, (String) newValue);

            }
        });
        
      //add Listener to filterField
        filterInput_team.textProperty().addListener(new ChangeListener<Object>() {
            public void changed(ObservableValue<?> observable, Object oldValue, Object newValue) {
            	filterUserListByTeam((String) oldValue, (String) newValue);

            }
        });
        
        id_user.setCellValueFactory(
    		    new PropertyValueFactory<User,Integer>("id_user")
    		);
		
		nom_user.setCellValueFactory(
		    new PropertyValueFactory<User,String>("nom_user")
		);
		
		prenom_user.setCellValueFactory(
		    new PropertyValueFactory<User,String>("prenom_user")
		);
		
		mail_user.setCellValueFactory(
		    new PropertyValueFactory<User,String>("mail_user")
		);
		
		isAdmin.setCellValueFactory(
		    new PropertyValueFactory<User,String>("isAdmin")
		);
		
		name_team.setCellValueFactory(
			new PropertyValueFactory<User,String>("name_team")
		);
		
		if (usersAsList != null) {
			tableViewUsers.setItems(items);
			lastID = items.get(items.size()-1).getId_user();
		}
		
	}

	public void loadComboBoxisAdmin () {
		new Thread(){
			public void run() {
				Platform.runLater(() -> comboBoxisAdmin.getItems().add(new KeyValuePair(0, "Non")));
				Platform.runLater(() -> comboBoxisAdmin.getItems().add(new KeyValuePair(1, "Oui")));
                 
                 try {
                     Thread.sleep(1000); 
                 } catch(InterruptedException ex) {
                     Thread.currentThread().interrupt();
                 } 
             }
         }.start();
		
	}
	
	public void loadComboBoxTeam() {

		new Thread(){
			public void run() {
				if (teams != null) {
					for (Team t : teams) {
						 Platform.runLater(() -> comboBoxteam.getItems().add(new KeyValuePair(t.getId_team(), t.getName_team())));
					}
				}

                 try {
                     Thread.sleep(1000); 
                 } catch(InterruptedException ex) {
                     Thread.currentThread().interrupt();
                 } 
             }
         }.start();
	}
	
	//Filtre de la table User par le pr�nom, nom et l'id 
    public void filterUserList(String oldValue, String newValue) {
        ObservableList<User> filteredList = FXCollections.observableArrayList();
        if(filterInput == null || (newValue.length() < oldValue.length()) || newValue == null) {
        	tableViewUsers.setItems(items);
        }
        else {
            newValue = newValue.toUpperCase();
            for(User user : tableViewUsers.getItems()) {
                String filterNomUser = user.getNom_user();
                String filterPrenomuser = user.getPrenom_user();
                String filterIdUser = String.valueOf(user.getId_user());
                
                if(filterNomUser.toUpperCase().contains(newValue) || filterPrenomuser.toUpperCase().contains(newValue) || filterIdUser.toUpperCase().contains(newValue)) {
                    filteredList.add(user);
                }
            }
            tableViewUsers.setItems(filteredList);
        }
    }
    
  //Filtre de la table User par le nom de son �quipe 
    public void filterUserListByTeam(String oldValue, String newValue) {
        ObservableList<User> filteredList = FXCollections.observableArrayList();
        if(filterInput == null || (newValue.length() < oldValue.length()) || newValue == null) {
        	tableViewUsers.setItems(items);
        }
        else {
            newValue = newValue.toUpperCase();
            for(User user : tableViewUsers.getItems()) {
                String filterNomEquipe = user.getName_team();
                
                if(filterNomEquipe.toUpperCase().contains(newValue)) {
                    filteredList.add(user);
                }
            }
            tableViewUsers.setItems(filteredList);
        }
    }
	
    public void btnAjouter_OnAction(ActionEvent event) {
		
		if (txtField_prenom.getText().isEmpty()) {
			label_saisiePrenom.setText("Champ obligatoire");
		}
		if (txtField_mail.getText().isEmpty()) {
			label_saisieEmail.setText("Champ obligatoire");
		}
		if (txtField_nom.getText().isEmpty()) {
			label_saisieNom.setText("Champ obligatoire");
		}
		if (PasswordFieldmdp.getText().isEmpty()) {
			label_saisiemdp.setText("Champ obligatoire");
		}

		if (!(comboBoxteam.getSelectionModel().selectedItemProperty().get().getValue().isEmpty() || comboBoxisAdmin.getSelectionModel().selectedItemProperty().get().getValue().isEmpty() || comboBoxisAdmin.getValue().equals("Choix Admin") || comboBoxteam.getValue().equals("Choix Equipe") || txtField_nom.getText().isEmpty() || txtField_mail.getText().isEmpty() || txtField_prenom.getText().isEmpty() || PasswordFieldmdp.getText().isEmpty())) {

			User user = new User();
		
			if (lastID == items.get(items.size()-1).getId_user()) {
				user = new User (
						lastID + 1,
						txtField_nom.getText(),
						txtField_prenom.getText(),
						txtField_mail.getText(),
						PasswordFieldmdp.getText(),
						comboBoxisAdmin.getSelectionModel().selectedItemProperty().get().getKey(),
						comboBoxteam.getSelectionModel().selectedItemProperty().get().getKey(),
						comboBoxteam.getSelectionModel().selectedItemProperty().get().getValue()
				);
			} else {
				user = new User (
						lastID,
						txtField_nom.getText(),
						txtField_prenom.getText(),
						txtField_mail.getText(),
						PasswordFieldmdp.getText(),
						comboBoxisAdmin.getSelectionModel().selectedItemProperty().get().getKey(),
						comboBoxteam.getSelectionModel().selectedItemProperty().get().getKey(),
						comboBoxteam.getSelectionModel().selectedItemProperty().get().getValue()
				);
			}
			

			items.add(user);
			userService.addUser(comboBoxisAdmin.getSelectionModel().selectedItemProperty().get().getKey(), txtField_mail.getText(), PasswordFieldmdp.getText(), txtField_nom.getText(), txtField_prenom.getText(), comboBoxteam.getSelectionModel().selectedItemProperty().get().getKey());
			
			label_saisiePrenom.setText("");
			label_saisieEmail.setText("");
			label_saisieNom.setText("");
			label_saisiemdp.setText("");
			
			txtField_nom.setText("");
			txtField_prenom.setText("");
			txtField_mail.setText("");
			PasswordFieldmdp.setText("");
			comboBoxisAdmin.setValue(new KeyValuePair(0, "Choix Admin"));
			comboBoxteam.setValue(new KeyValuePair(0, "Choix Equipe"));
		}
	}

	public void btnSupprimer_OnAction(ActionEvent event) {
		User userSelected =  tableViewUsers.getSelectionModel().selectedItemProperty().get();
		int indexRowSelected = tableViewUsers.getSelectionModel().selectedIndexProperty().get();
		
		lastID = items.get(items.size()-1).getId_user() +1;
		
		items.remove(indexRowSelected);
		
		userService.deleteUser(userSelected.getId_user());
		
		txtField_nom.setText("");
		txtField_prenom.setText("");
		txtField_mail.setText("");
		PasswordFieldmdp.setText("");
		comboBoxteam.setValue(new KeyValuePair(0, "Choix Equipe"));
		comboBoxisAdmin.setValue(new KeyValuePair(0, "Choix Admin"));
	}
	
	public void tableViewUsers_OnMouseCliked (MouseEvent mouseevent){
		
		User userSelected =  tableViewUsers.getSelectionModel().selectedItemProperty().get();
		
		txtField_nom.setText(userSelected.getNom_user());
		txtField_prenom.setText(userSelected.getPrenom_user());
		txtField_mail.setText(userSelected.getMail_user());
		PasswordFieldmdp.setText(userSelected.getMdp_user());
		comboBoxteam.setValue(new KeyValuePair(userSelected.getId_user(), userSelected.getName_team()));
		
		//TODO SET COMBO BOX IS ADMIN 
     }

	
	public void btnEditer_OnAction(ActionEvent event) {
		
		User userSelected =  tableViewUsers.getSelectionModel().selectedItemProperty().get();
		
		if (!comboBoxteam.getSelectionModel().selectedItemProperty().get().getValue().isEmpty() && !comboBoxisAdmin.getValue().equals("Choix Admin") && !txtField_nom.getText().isEmpty() && !txtField_prenom.getText().isEmpty() && !txtField_mail.getText().isEmpty() && !PasswordFieldmdp.getText().isEmpty() && !comboBoxteam.getValue().equals("Choix Equipe")) {
			userSelected.setMail_user(txtField_mail.getText());
			userSelected.setPrenom_user(txtField_prenom.getText());
			userSelected.setNom_user(txtField_nom.getText());
			userSelected.setMdp_user(PasswordFieldmdp.getText());
			userSelected.setName_team(comboBoxteam.getSelectionModel().selectedItemProperty().get().getValue());
			userSelected.setIsAdmin(comboBoxisAdmin.getSelectionModel().selectedItemProperty().get().getKey());
			
			txtField_nom.setText("");
			txtField_prenom.setText("");
			txtField_mail.setText("");
			PasswordFieldmdp.setText("");
			comboBoxteam.setValue(new KeyValuePair(0, "Choix Equipe"));
			comboBoxisAdmin.setValue(new KeyValuePair(0, "Choix Admin"));
			
			tableViewUsers.setItems(items);
			tableViewUsers.refresh();
		}
	}
	
	public void btn_Back_OnAction(ActionEvent event) {
		try {
			Stage stage;
			Parent home_page_parent;
			
			stage = (Stage) btn_Back.getScene().getWindow();
			
			home_page_parent =  FXMLLoader.load(Main.class.getResource("/Views/HomePage.fxml"));
			
			Scene home_page_scene = new Scene(home_page_parent);
			
			stage.setScene(home_page_scene);
			stage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
