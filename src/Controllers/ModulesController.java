package Controllers;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import Models.KeyValuePair;
import Models.Module;
import Models.Project;
import Services.IncidentService;
import Services.ModuleService;
import Services.ProjectService;
import application.Main;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class ModulesController implements Initializable{

	@FXML
	TableView<Module> tableViewModules;
	
	@FXML
	TableColumn<Module, Integer> id_module;
	
	@FXML
	TableColumn<Module, String> name_module;
	
	@FXML
	TableColumn<Module, String> libelle_pro;
	
	@FXML
	TextField filterInput;
	
	@FXML
	TextField txtField_nameModule;
	
	@FXML
	TextArea txtArea_Description;
	
	@FXML
	Label label_saisieName;
	
	@FXML
	Label label_saisieDescription;
	
	@FXML
	Label labelavancementModule;
	
	@FXML 
	private ProgressBar progress_module;
	
	@FXML 
	Button btnAjouter;
	
	@FXML 
	Button btnEditer;
		
	@FXML 
	Button btnSupprimer;
	
	@FXML
	Button btn_Back;
	
	@FXML
	TextField txtField_progress;
	
	@FXML
	TextField filterInput_proj;
	
	@FXML
	ComboBox<KeyValuePair> comboBoxProjects;
	
	int lastID = 0;
	
	ModuleService moduleService = new ModuleService();
	ProjectService projectService = new ProjectService();
	IncidentService incidentService = new IncidentService();

	List<Module> modules = moduleService.getAllModules();
	List <Project> projects = projectService.getAllProjects();
	
	final ObservableList<Module> items = FXCollections.observableArrayList(modules);

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		loadComboBoxProjects();
		
		//add Listener to filterField
		filterInput.textProperty().addListener(new ChangeListener<Object>() {
			public void changed(ObservableValue<?> observable, Object oldValue, Object newValue) {
				filterModuleList((String) oldValue, (String) newValue);
			}
		});

		//add Listener to filterField
		filterInput_proj.textProperty().addListener(new ChangeListener<Object>() {
			public void changed(ObservableValue<?> observable, Object oldValue, Object newValue) {
				filterModuleListByProjectLibelle((String) oldValue, (String) newValue);
			}
		});
				
        id_module.setCellValueFactory(
			    new PropertyValueFactory<>("id_module")
			);
		
        name_module.setCellValueFactory(
			    new PropertyValueFactory<>("name_module")
			);
        
        libelle_pro.setCellValueFactory(
			    new PropertyValueFactory<>("libelle_pro")
			);
        
        //Si liste nulle inutile de charger les donn�es, cela fera tout casser
        if (modules != null) {
        	tableViewModules.setItems(items);
        	lastID = items.get(items.size()-1).getId_module();
        }

	}

	//Fonction permettant de charger les donn�es pour la comboBoxProjects
	public void loadComboBoxProjects() {

		new Thread(){
			public void run() {
				if (projects != null) {
					for (Project p : projects) {
						 Platform.runLater(() -> comboBoxProjects.getItems().add(new KeyValuePair(p.getId_project(), p.getLibelle_project())));
					}
				}
				
                 try {
                     Thread.sleep(1000); 
                 } catch(InterruptedException ex) {
                     Thread.currentThread().interrupt();
                 } 
             }
         }.start();
	}
	
	//Filter the table View By the name of the module or the ID 
    public void filterModuleList(String oldValue, String newValue) {
        ObservableList<Module> filteredList = FXCollections.observableArrayList();
        if(filterInput == null || (newValue.length() < oldValue.length()) || newValue == null) {
        	tableViewModules.setItems(items);
        }
        else {
            newValue = newValue.toUpperCase();
            for(Module mod : tableViewModules.getItems()) {
                String filterNameModule = mod.getName_module();
                String filterIdModule = String.valueOf(mod.getId_module());
                
                if(filterNameModule.toUpperCase().contains(newValue) || filterIdModule.toUpperCase().contains(newValue)) {
                    filteredList.add(mod);
                }
            }
            tableViewModules.setItems(filteredList);
        }
    }
    
  //Filter the table View By the name of the project
    public void filterModuleListByProjectLibelle(String oldValue, String newValue) {
        ObservableList<Module> filteredList = FXCollections.observableArrayList();
        if(filterInput == null || (newValue.length() < oldValue.length()) || newValue == null) {
        	tableViewModules.setItems(items);
        }
        else {
            newValue = newValue.toUpperCase();
            for(Module mod : tableViewModules.getItems()) {
                String filterNameProject = mod.getLibelle_pro();
                
                if(filterNameProject.toUpperCase().contains(newValue)) {
                    filteredList.add(mod);
                }
            }
            tableViewModules.setItems(filteredList);
        }
    }
    
    public void btnAjouter_OnAction(ActionEvent event) {
		
		if (txtField_nameModule.getText().isEmpty()) {
			label_saisieName.setText("Champ obligatoire");
		}

		if (txtArea_Description.getText().isEmpty()) {
			label_saisieDescription.setText("Champ obligatoire");
		}

		if (!(txtArea_Description.getText().isEmpty() || txtField_nameModule.getText().isEmpty())) {

			Module mod = new Module();
			float prog;
			
			if (txtField_progress.getText().isEmpty()) {
				
				prog = 0;
			} else {
				prog = Float.parseFloat(txtField_progress.getText());
			}
			
			if (lastID == items.get(items.size()-1).getId_module()) {
				mod = new Module (
						lastID +1,
						txtField_nameModule.getText(),
						prog,
						txtArea_Description.getText(),
						comboBoxProjects.getSelectionModel().selectedItemProperty().get().getKey(),
						comboBoxProjects.getSelectionModel().selectedItemProperty().get().getValue()
				);
			} else {
				mod = new Module (
						lastID,
						txtField_nameModule.getText(),
						prog,
						txtArea_Description.getText(),
						comboBoxProjects.getSelectionModel().selectedItemProperty().get().getKey(),
						comboBoxProjects.getSelectionModel().selectedItemProperty().get().getValue()
				);
			}

			//Ajout de l'objet dans la ObservableList
			items.add(mod);
			
			//Appel du bon webService 
			moduleService.addModule(txtField_nameModule.getText(), txtArea_Description.getText(), prog, comboBoxProjects.getSelectionModel().selectedItemProperty().get().getKey());
			
			//REFRESH DES CHAMPS
			label_saisieName.setText("");
			label_saisieDescription.setText("");
			txtField_nameModule.setText("");
			txtArea_Description.setText("");
			txtField_progress.setText("");
			comboBoxProjects.setValue(new KeyValuePair (0, "Projet"));
		}
	}

	public void btnEditer_OnAction(ActionEvent event) {
		
		//R�cup�ration de l'item s�lectionn� sur la tableView
		Module moduleSelected = tableViewModules.getSelectionModel().selectedItemProperty().get();
		
		//Si champs non vides
		if (!txtField_nameModule.getText().isEmpty() && !txtArea_Description.getText().isEmpty() && !txtField_progress.getText().isEmpty()) {
			
			//Modification des champs sur l'objet s�lectionn�
			moduleSelected.setName_module(txtField_nameModule.getText());
			moduleSelected.setDescription_module(txtArea_Description.getText());
			moduleSelected.setProgress_module(Float.parseFloat(txtField_progress.getText()));
			
			//Apppel du web service
			moduleService.updateModule(txtField_nameModule.getText(), txtArea_Description.getText(), Float.parseFloat(txtField_progress.getText()), moduleSelected.getId_module());
			
			txtField_nameModule.setText("");
			txtArea_Description.setText("");
			txtField_progress.setText("");
			comboBoxProjects.setValue(new KeyValuePair (0, "Projet"));
			
			//Refresh de la tableView
			tableViewModules.setItems(items);
			tableViewModules.refresh();
		}
	}
    
    public void btnSupprimer_OnAction(ActionEvent event) {
    	
    	//r�cup�ration de l'indice et l'item s�lectionn� sur la tableView
    	Module moduleSelected = tableViewModules.getSelectionModel().selectedItemProperty().get();;
		int indexRowSelected = tableViewModules.getSelectionModel().selectedIndexProperty().get();
		
		lastID = items.get(items.size()-1).getId_module() + 1;
		
		//On enl�ve l'item s�lectionn� de l'ObservableList qui enl�vera l'item de la tableView
		items.remove(indexRowSelected);
		
		//Appel du webservice permettant la suppression du Module ainsi que tous les incidents li�s au module
		moduleService.deleteModule(moduleSelected.getId_module());
		incidentService.deleteIncidentByModule(moduleSelected.getId_module());
		
		//RFRESH DES CHAMPS
		txtField_nameModule.setText("");
		txtArea_Description.setText("");
		txtField_progress.setText("");
		progress_module.setProgress(0);
		comboBoxProjects.setValue(new KeyValuePair (0, "Projet"));
    }
    
	public void tableViewModules_OnMouseClicked (MouseEvent mouseevent){
		
		//Lors du click sur une cellule de la tableView on rempli les champs avec l'item s�lectionn�
		
		Module moduleSelected = tableViewModules.getSelectionModel().selectedItemProperty().get();
		
		txtField_nameModule.setText(moduleSelected.getName_module());
		txtArea_Description.setText(moduleSelected.getDescription_module());
		txtField_progress.setText(String.valueOf(moduleSelected.getProgress_module()));
		comboBoxProjects.setValue(new KeyValuePair (moduleSelected.getId_pro(), moduleSelected.getLibelle_pro()));
		
		new Thread(){
			public void run() {
            	 
                 Platform.runLater(() -> progress_module.setProgress(moduleSelected.getProgress_module()/100));
                 
                 try {
                     Thread.sleep(1000); 
                 } catch(InterruptedException ex) {
                     Thread.currentThread().interrupt();
                 } 
             }
         }.start();
		
	}

	//Bouton retour permettant de revenir sur la HomePage
	public void btn_Back_OnAction(ActionEvent event) {
		try {
			Stage stage;
			Parent home_page_parent;
			
			stage = (Stage) btn_Back.getScene().getWindow();
			
			home_page_parent =  FXMLLoader.load(Main.class.getResource("/Views/HomePage.fxml"));
			
			Scene home_page_scene = new Scene(home_page_parent);
			
			stage.setScene(home_page_scene);
			stage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
